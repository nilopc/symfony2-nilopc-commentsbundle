<?php


namespace Nilopc\CommentsBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\ExecutionContext;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
abstract class BaseComment
{
  /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	protected $id;


    /**
     * @var text $comment
     *
     * @ORM\Column(name="comment", type="text")
     * @Assert\NotBlank()
     */
	protected $comment = NULL;

	protected $item_object;
	protected $user_object;

    /**
     * @var boolean $active
     *
     * @ORM\Column(name="active", type="boolean")
     * @Assert\Type(type="bool")
     */	
    protected $active = false;

    /**
     * @var \DateTime
     *    
     * @ORM\Column(name="date_created", type="datetime")
     * @Assert\DateTime()     
     */
    protected $date_created;

    protected $parent_id=NULL;


    public function __construct()
    {
        $this->parent_id = new ArrayCollection();
    	$this->date_created = new \DateTime();
    }

    public function __toString()
    {
        return $this->id;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }    

    /**
     * Get comment
     *
     * @return string $comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set comment
     *
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment=$comment;
    }


    /**
     * Set date_created
     *
     * @param \DateTime $date_created
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
    }

    /**
     * Get date_created
     *
     * @return \DateTime $date_created
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Get active
     *
     * @return boolean $active
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set active
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active=$active;
    }


    /**
     * Get parent_id
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parent_id;
    }    


    /**
     * Set parent_id
     *
     * @param integer $parent_id
     */
    public function setParentId($parent_id)
    {
        $this->parent_id=$parent_id;
    }    
}