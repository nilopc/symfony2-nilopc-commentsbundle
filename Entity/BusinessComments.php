<?php

namespace Nilopc\CommentsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\ExecutionContext;
use Doctrine\Common\Collections\ArrayCollection;

use Nilopc\CommentsBundle\Model\BaseComment;

/**
 * @ORM\Entity
 * @ORM\Table(name="business_comments")
 */
class BusinessComments extends BaseComment
{

    /** 
     * @ORM\ManyToOne(targetEntity="Nilopc\UserBundle\Entity\User") 
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     * })       
     */
    protected $user_object;
   


    /** 
     * @ORM\ManyToOne(targetEntity="Nilopc\BusinessBundle\Entity\Business") 
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="item_id", referencedColumnName="id", onDelete="CASCADE")
     * })       
     */
    protected $item_object;
   


    /**
     * Set item_object
     *
     * @param Nilopc\BusinessBundle\Entity\Business $item_object
     */
    public function setItemObject(\Nilopc\BusinessBundle\Entity\Business $item_object)
    {
        $this->item_object = $item_object;
    }


    /**
     * Get item_object
     *
     * @return Nilopc\BusinessBundle\Entity\Business
     */
    public function getItemObject()
    {
        return $this->item_object;
    }


    /**
     * Set user_object
     *
     * @param Nilopc\UserBundle\Entity\User $user_object
     */
    public function setUserObject(\Nilopc\UserBundle\Entity\User $user_object)
    {
        $this->user_object = $user_object;
    }    

    
    /**
     * Get user_object
     *
     * @return Nilopc\UserBundle\Entity\User
     */
    public function getUserObject()
    {
    	return $this->user_object;
    }


}