<?php

namespace Nilopc\CommentsBundle\Controller;

use Symfony\Component\HttpFoundation\Request,
    Symfony\Bundle\FrameworkBundle\Controller\Controller,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Route,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Nilopc\BusinessBundle\Entity\Business, 
    Nilopc\UserBundle\Entity\User, 
    Nilopc\CommentsBundle\Entity\BusinessComments as Comment, 
    Nilopc\CommentsBundle\Form\Type\CommentType;     

class CommentController extends Controller
{
    /**
     * @Route("/_examples/commentbundle", name="commentbundle_example")
     * @Template()
     */
    public function exampleAction()
    {
    	//let's say we know these somehow
    	$user_id=16; 	// SHOULD TOTALLY come from the user's session
		$page_id=6;		// should come from somewhere, database or url.. you decide


		/** Would be nice to check if the current user is allowed to comment **/


    	//create and retrieve the entity objects
    	$comment = new Comment();
        $user=$this->getDoctrine()->getRepository('NilopcUserBundle:User')->find($user_id);    	
        $item=$this->getDoctrine()->getRepository('NilopcBusinessBundle:Business')->find($page_id);    			


		//Pass $comment to the CommentType to generate the form
 		$form = $this->createForm(new CommentType($item,$user),$comment);


        //Check for data to save (POST) or show form (GET)
        $request = Request::createFromGlobals();
        if ($request->getMethod() == 'POST')
    	{
            $form->bindRequest($request);
            if ($form->isValid())
            {

            	//get form ID values for User and Item
            	$postData = $this->get('request')->request->get('comments');
            	$form_user_id=$postData['user_object'];
				$form_item_id=$postData['item_object'];


		        $user=$this->getDoctrine()->getRepository('NilopcUserBundle:User')->find($form_user_id);    	
		        $item=$this->getDoctrine()->getRepository('NilopcBusinessBundle:Business')->find($form_item_id);          

            	/* 
            		$user->getId()==$user_id check is here for very crutial security purposes.
            		Without it, it would be dead easy to post as someone else fiddling post values with Firebug or similar.
            	*/	
            	if( $user && $item && $user->getId()==$user_id )	
            	{
            		//redo the user and item entity relationship
            		$comment->setItemObject($item);
            		$comment->setUserObject($user);

            		//persist data
                	$em = $this->getDoctrine()->getEntityManager();
                	$em->persist($comment);
                	$em->flush(); 

               		$this->get('session')->setFlash('success','flash.message.new.comment');
                	return $this->redirect($this->generateUrl('commentbundle_example'));
                }	
                else
                {
            	  	throw $this->createNotFoundException('Page Not Found');
                }
            }
        }
        
        return array
        (
            'form'  =>  $form->createView()
        );       	
    }
}



     