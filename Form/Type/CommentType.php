<?php

namespace Nilopc\CommentsBundle\Form\Type;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilder,
    Doctrine\ORM\EntityRepository;

class CommentType extends AbstractType
{
    private $item_object;
    private $user_object;    


    public function __construct($item_object,$user_object)
    {
        $this->item_object=$item_object; 
        $this->user_object=$user_object;
    }


    public function buildForm(FormBuilder $builder, array $options)
    {
        //defining the whole comment bit.
        $builder->add('comment','textarea', array('label'  => 'comment',));

        $builder->add('user_object', 'hidden',array
        (
            'data' => $this->user_object->getId(),
            'property_path' => false,
        ));

        $builder->add('item_object', 'hidden',array
        (
            'data' => $this->item_object->getId(),
            'property_path' => false,
        ));
    }

    public function getName()
    {
        return 'comments';
    }
}


