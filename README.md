# Nilopc\CommentBundle README

## Installation & Configuration


## Usage Example

Let's see how to get it to work in 3 easy steps. 

### Step 1: Writing your Comments Entity

It's pretty straight forward. You can even copy and paste the following example. Remember to replace 
Nilopc\UserBundle\Entity\User, Nilopc\PageBundle\Entity\Page and Nilopc\CommentsBundle\Entity\PageComments for whatever suits you best.



      <?php

        namespace Nilopc\CommentsBundle\Entity;

        use Doctrine\ORM\Mapping as ORM;
        use Symfony\Component\Validator\Constraints as Assert;
        use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
        use Symfony\Component\Validator\ExecutionContext;
        use Doctrine\Common\Collections\ArrayCollection;

        use Nilopc\CommentsBundle\Model\BaseComment;

        /**
         * @ORM\Entity
         * @ORM\Table(name="page_comments")
         */
        class PageComments extends BaseComment
        {

            /** 
             * @ORM\ManyToOne(targetEntity="Nilopc\UserBundle\Entity\User") 
             * @ORM\JoinColumns({
             *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
             * })       
             */
            protected $user_object;
           


            /** 
             * @ORM\ManyToOne(targetEntity="Nilopc\PageBundle\Entity\Page") 
             * @ORM\JoinColumns({
             *   @ORM\JoinColumn(name="item_id", referencedColumnName="id", onDelete="CASCADE")
             * })       
             */
            protected $item_object;
           


            /**
             * Set item_object
             *
             * @param Nilopc\PageBundle\Entity\Page $item_object
             */
            public function setItemObject(\Nilopc\PageBundle\Entity\Page $item_object)
            {
                $this->item_object = $item_object;
            }


            /**
             * Get item_object
             *
             * @return Nilopc\PageBundle\Entity\Page
             */
            public function getItemObject()
            {
                return $this->item_object;
            }


            /**
             * Set user_object
             *
             * @param Nilopc\UserBundle\Entity\User $user_object
             */
            public function setUserObject(\Nilopc\UserBundle\Entity\User $user_object)
            {
                $this->user_object = $user_object;
            }    

            
            /**
             * Get user_object
             *
             * @return Nilopc\UserBundle\Entity\User
             */
            public function getUserObject()
            {
                return $this->user_object;
            }


        }




### Step 2: Relating the new entity with the user and commented item

We need to finish up setting the entity relationships. Let's say we're using a Page and a User entity, just copy and paste the following inside each of the entity classes.



    	// Edit Nilopc\UserBundle\Entity\User.php and paste this inside the class
        /** @ORM\OneToMany(targetEntity="Nilopc\CommentsBundle\Entity\PageComments", mappedBy="user_object", orphanRemoval=true, cascade={"persist","remove"}) */
        protected $comment;    





    	// Edit Nilopc\PageBundle\Entity\Page.php and paste this inside the class

        /** @ORM\OneToMany(targetEntity="Nilopc\CommentsBundle\Entity\PageComments", mappedBy="user_object", orphanRemoval=true, cascade={"persist","remove"}) */
        protected $comment;     
  


### Step 3: Entity to real schema

Now create the entity's schema.

	php app/console doctrine:schema:update --force

And don't forget to update your entities

    php app/console d:g:entities Nilopc\UserBundle

    php app/console d:g:entities Nilopc\PageBundle
    

### Step 4: The controller

Check out exampleController.php for the code... it's meant to be edited. Read through the code's comments!!   